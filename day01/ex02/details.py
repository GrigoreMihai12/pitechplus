#! /usr/bin/python3
import sys
from datetime import date
import time
def get_details():
    print("Python " + str(sys.version_info[0]) + " " + str(sys.version_info[1]) + " " + str(sys.version_info[2]))
    print("Current date and time: " + date.today().strftime('%d-%m-%Y') + " " + time.strftime("%H:%M:%S")) 
get_details()
