A virtual environment is a tool that helps to keep dependencies required by different projects separate by creating isolated python virtual environments for them.
Virtual environment should be used whenever you work on any Python based project.
A module named virtualenv which is a tool to create isolated Python environments.
virtualenv creates a folder which contains all the necessary executables to use the packages that a Python project would need.
