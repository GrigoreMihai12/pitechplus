#! /usr/bin/python3

try:
    numbers = input()
    if not numbers:
        raise EOFError ('No input provided')
    numbers = numbers.split(" ")
    if len(numbers) != 2:
        raise ValueError ('Invalid interval')
    a = int(numbers[0])
    b = int(numbers[1])
    if a > b:
        raise ValueError ('Invalid interval')
    for i in range(a, b+1):
        print(str(i), end = " ")
except EOFError as e:
    print(e)
except ValueError as e1:
    print(e1)
except TypeError as e2:
    print(e2)

