#! /usr/bin/python3

import time
import random
count = 10
while count:
    p = random.randint(1,10)
    try:
        if p >= 4 and p <= 6:
            raise ValueError ('Number between 4 and 6')
        print(p)
    except ValueError as e:
        print(e)
    time.sleep(1)
    count -= 1
