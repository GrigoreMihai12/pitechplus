#! /usr/bin/python3
def count_words():
    try:
        sent = input()
        sent1 = sent.split(" ")
        count = 0
        for i in sent1:
            if i:
                count += 1
        return count
    except EOFError as ex:
        print("No input provided")
        raise ex
nr = count_words()
if nr == 0:
    print("No input provided")
else:
    print(str(nr))
