#! /usr/bin/python

def create_types():
    try:
        file1 = open("demo.txt","r")
        content = file1.read(2048)
        content = content.split(",")
        if len(content) == 0:
            raise ValueError("Invalid input")
        index = 0
        list1=[]
        dic = {}
        for i in content:
            p = int(i)
            list1.append(p)
            dic[index] = p
            index += 1
        print(list1)
        tuple1 =tuple(list1)
        print(tuple1)
        print(dic)
    except ValueError as e:
        print(e)
    except:
        print("Invalid input")


create_types()
