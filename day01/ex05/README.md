A list is a collection of ordered data. A tuple is an ordered collection of data. A dictionary is an unordered collection of data that stores data in key-value pairs.
Lists are mutable and declared with square braces. Tuples are imutable and are enclosed within paranthesis. Dictionaries are mutable and keys do not allow duplicates and are enclosed in curly brackets in the form of key-value pairs.
