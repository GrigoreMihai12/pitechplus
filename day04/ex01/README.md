Cloud Storage stores data in objects and collects objects inside buckets. Buckets have a lot in common with directories, but there’s one major difference: Buckets can’t be nested. That is, you can’t organize buckets into a hierarchy in the way that you can organize directories.
All load/store/delete operations involving Cloud Storage must identify at least one target bucket.
Every bucket has a globally unique name, a storage class, and a geographic location.
A project can create/delete buckets at most once every two seconds.
