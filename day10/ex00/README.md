Pub/Sub is used for streaming analytics and data integration pipelines to ingest and distribute data. 
It is equally effective as messaging-oriented middleware for service integration or as a queue to parallelize tasks.
Pub/Sub enables you to create systems of event producers and consumers, called publishers and subscribers.
Publishers communicate with subscribers asynchronously by broadcasting events, rather than by synchronous remote procedure calls