#! /usr/bin/python3
from google.cloud import pubsub_v1

project_id = "atlantean-force-327605"
subscription_id = "projects/atlantean-force-327605/topics/starting-with-UI"
timeout = 5.0

subscriber = pubsub_v1.SubscriberClient()
subscription_path = subscriber.subscription_path(project_id, subscription_id)

def callback(message: pubsub_v1.subscriber.message.Message) -> None:
    print(message)
    message.ack()

streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback)

with subscriber:
    try:
        streaming_pull_future.result(timeout=timeout)
    except Exception as e:
        print(
            "Listening for messages on {subscription_path} threw an exception: {e}."
        )
        streaming_pull_future.cancel()
        streaming_pull_future.result() 