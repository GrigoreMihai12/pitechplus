#! /usr/bin/python3
from google.cloud import pubsub_v1

project_id = "atlantean-force-327605"
topic_id = "starting-with-UI"

publisher = pubsub_v1.PublisherClient()
topic_path = publisher.topic_path(project_id, topic_id)

for n in range(0, 5):
    data = "This is sent from code message id:" + topic_path + "."
    data = data.encode("utf-8")
    future = publisher.publish(
        topic_path, data, origin="python-sample", username="gcp"
    )
    print(future.result())