Google Cloud datastore:
Part of the Google Cloud Platform walled garden.
Pay for read/writes to the datastore, in addition to storage.
Not many libraries exist.
Google Cloud SQL:
Secure access is complicated to set up - This is the nature of secure access, but it can be frustrating spending much time simply configuring access.
No custom IP - It isn't possible to customize an instance's IP. This makes it harder to replace an instance since a new or clone instance always gets a new IP.
Cost tends to increase - This is the nature of cloud hosting, but over time costs creep up as utilization increases.