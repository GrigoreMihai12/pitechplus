IPv4 addresses have 4 bytes (32 bits).
Internet Protocol, or IP, is a protocol used by private and public networks to facilitate communication between devices within the network. 
All types of network, from the World Wide Web to small private network, depend on assigned IP addresses to dictate where information goes. 
An IP address is set of unique 8-bit numbers assigned to a device that connect to a network.
Your IP address is like your home address but for internet-capable devices. Instead of “mailing” a letter, you’re “mailing” information.
There are two types of IP addressing standards, IPv4 and IPv6. IPv4 is the most widely used and familiar type of IP address, but with IPv4 address space running out,
IPv6 is in line to replace it in the future.
IP addresses (using the IPv4 standard) are divided into 5 classes:
Class A Starting Adress: 0.0.0.0 - Ending Adress: 127.255.255.255 Purpose: These are designed to be used in very large companies like Google.
Class B Starting Adress: 128.0.0.0 - Ending Adress: 191.255.255.255 Purpose: These are designed to be used in medium-sized companies
Class C Starting Adress: 192.0.0.0 - Ending Adress: 223.255.255.255 Purpose: They designed to be used in small-sized companies.
Class D Starting Adress: 224.0.0.0 - Ending Adress: 239.255.255.255 Purpose: They are not used in the public sector, instead being reserved for multicast addressing
Class E Starting Adress: 240.0.0.0 - Ending Adress: 255.255.255.255 Purpose: They are also not used in the public sector, instead being reserved for scientific studies.

