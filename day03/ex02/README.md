A virtual machine (VM) is a virtual environment that functions as a virtual computer 
system with its own CPU, memory, network interface, and storage, created on a physical hardware system.
VM are isolated from the rest of the system, and multiple VMs can exist on a single piece of hardware.
The virtual machine runs as a process in an application window, similar to any other application, on the
operating system of the physical machine.
VMs can run multiple operating system environments on a single physical computer, saving physical space and time

P.S.: For the requested virtual machine, I get an error that asks me for a premium account.