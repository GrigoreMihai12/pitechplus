Firewall rules are defined at the network level, and only apply to the network where
they are created; however, the name you choose for each of them must be unique to the project.
A firewall rule can contain either IPv4 or IPv6 ranges, but not both
VPC firewall rules let you allow or deny connections to or from your virtual machine (VM) instances based on a configuration that you specify.
Enabled VPC firewall rules are always enforced, protecting your instances regardless of their configuration and operating system, even if they have not started up.
Every VPC network functions as a distributed firewall. 
While firewall rules are defined at the network level, connections are allowed or denied on a per-instance basis.
You can think of the VPC firewall rules as existing not only between your instances and other networks, but also between individual instances within the same network.
Examples:
1.You need to create two rules for the 'Home segment' interface.
First, we will create a Permit rule where you define the source IP address (the computer's IP address to be allowed to access) and the TCP protocol type.
2.You need to create one rule for 'Home segment'. We will create a Deny rule where we set the source IP address
(the computer's IP address which access will be denied) and the TCP protocol type.