gcloud compute networks create net-03 \
	--subnet-mode = custom\
	--bgp-routing-mode = global\
	--mtu = 1460
gcloud compute networks subnets create subnet01\
	--network = net-03\
	--range = 11.0.0.0/1\
	--region = us-central1
gcloud compute networks subnet create subnet02\
	--network = net-03\
	--range = 12.0.0.0/2\
	--region = us-central2
gcloud compute instances create my-vm-us-2\
	--region = us-central1\
	--hostname = subnet01
gcloud compute instances create my-vm-us3\
	--region = us-central2\
	--hostname = subnet02


