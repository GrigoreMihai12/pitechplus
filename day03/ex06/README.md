A public IP address is an IP address that is used to access the Internet.
Public IP addresses can be routed on the Internet, unlike private addresses. 
The presence of a public IP address on your router or computer will allow you to organize your own server, remote access to your computer, video surveillance cameras,
and get access to them from anywhere on the global network.
With a public IP address, you can set up any home server to publish it on the Internet: Web (HTTP), VPN, media, game server, etc.
Private addresses are not routed on the Internet and no traffic can be sent to them from the Internet, they only supposed to work within the local network.
Private addresses include IP addresses from the following subnets:
1.Range from 10.0.0.0 to 10.255.255.255 — a 10.0.0.0 network with a 255.0.0.0 mask
2.Range from 172.16.0.0 to 172.31.255.255 — a 172.16.0.0 network with a 255.240.0.0
3.A 192.168.0.0 to 192.168.255.255 range, which is a 192.168.0.0 network masked by 255.255.0.0