It provides tools to fast-track the migration process from on-premise or other clouds to GCP. 
If a user is starting with the public cloud, then they can leverage these tools to seamlessly 
transfer existing applications from their data center, AWS, or Azure to GCP. Users can have their 
applications running on Compute Engine within minutes while the data migrate transparently in the background.
Google Compute Engine, as an Infrastructure as a Service(IaaS), offers and enables the user to execute workloads 
on the physical server of Google.