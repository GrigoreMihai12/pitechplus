Many Google Cloud resources can have internal IP addresses and external IP addresses.
Instances use these addresses to communicate with other Google Cloud resources and external systems.
Each VM instance network interface must have one primary internal IP address, can have one or more alias IP ranges, and can have one external IP address.
To communicate between instances on the same Virtual Private Cloud (VPC) network, you can use the internal IP address for the instance.
To communicate with the internet, you must use the instance's external IP address unless you have configured a proxy of some kind.
Similarly, you must use the instance's external IP address to connect to instances outside of the same VPC network unless the networks are connected in some way, 
like by using Cloud VPN. Both external and internal primary IP addresses can be either ephemeral or static.
A forwarding rule is required for network, global, and internal load balancing. 
The forwarding rule must have an external or internal IP address, depending on the load balancer you are using.
For network and global load balancing, you can create a regional or global forwarding rule and allocate a regional or global static external IP address,
respectively. For internal load balancing, assign an internal IP address.