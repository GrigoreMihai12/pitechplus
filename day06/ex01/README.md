All HTTP response status codes are separated into five classes or categories. The first digit of the status code defines the class of response, while the last two digits do not have any classifying or categorization role. There are five classes defined by the standard:
1xx informational response – the request was received, continuing process
(101 Switching Protocols
The requester has asked the server to switch protocols and the server has agreed to do so.)
2xx successful – the request was successfully received, understood, and accepted
(202 Accepted
The request has been accepted for processing, but the processing has not been completed. )
3xx redirection – further action needs to be taken in order to complete the request
(306 Switch Proxy
No longer used. Originally meant "Subsequent requests should use the specified proxy." )
4xx client error – the request contains bad syntax or cannot be fulfilled
(404 Not Found
The requested resource could not be found but may be available in the future. Subsequent requests by the client are permissible.)
5xx server error – the server failed to fulfil an apparently valid request
(500 Internal Server Error
A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.)