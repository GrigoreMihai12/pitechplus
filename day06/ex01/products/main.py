from flask import Flask, jsonify, abort, request, render_template

app = Flask(__name__)

tasks = [
    {
        'id': 1,
        'name': u'name1',
        'price': 10 
    },
    {
        'id': 2,
        'name': u'name2',
        'price': 10 
    }
]

@app.route('/list', methods=['GET'])
def get_tasks():
    return jsonify({'tasks': tasks})

@app.errorhandler(404)
@app.route('/find/<int:iduser>', methods=['GET'])
def get_task(iduser):
    task = [task for task in tasks if task['id'] == iduser]
    if len(task) == 0:
        render_template('404.html'), 404
    return jsonify({'task': task[0]})

@app.errorhandler(400)
@app.route('/add', methods=['POST'])
def create_task():
    if not request.json or not 'title' in request.json:
         render_template('400.html'), 400
    task = {
        'id': tasks[-1]['id'] + 1,
        'name': request.json['name'],
        'price':request.json['price']
    }
    tasks.append(task)
    return jsonify({'task': task}), 201

@app.errorhandler(400)
@app.errorhandler(404)
@app.route('/modify/<int:iduser>', methods=['PUT'])
def update_task(iduser):
    task = [task for task in tasks if task['id'] == iduser]
    if len(task) == 0:
        render_template('404.html'), 404
    if not request.json:
         render_template('400.html'), 400
    task[0]['name'] = request.json.get('name', task[0]['name'])
    task[0]['price'] = request.json.get('price', task[0]['price'])
    return jsonify({'task': task[0]})

@app.errorhandler(404)
@app.route('/delete/<int:iduser>', methods=['DELETE'])
def delete_task(iduser):
    task = [task for task in tasks if task['id'] == iduser]
    if len(task) == 0:
        render_template('404.html'), 404
    tasks.remove(task[0])
    return jsonify({'result': True})

if __name__ == '__main__':
    app.run(debug=True)