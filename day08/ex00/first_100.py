#! /usr/bin/python3

from google.cloud import bigquery

def query_stackoverflow():
    client = bigquery.Client()
    query_job = client.query(
        """
        SELECT score, text, creation_date
        FROM `bigquery-public-data.stackoverflow.comments`
        LIMIT 100"""
    )

    results = query_job.result()

    with open("result.txt", "w") as f:
        for row in results:
           f.write('Score: {:<5} '.format(str(row[0])) + 'Comment: {:<120}'.format(str(row[1].encode('utf-8'))[0:119] ) + 'Creation data: {:<10}'.format(str(row[2])) )
           f.write("\n")

if __name__ == "__main__":
    query_stackoverflow()