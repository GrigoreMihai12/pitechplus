#! /usr/bin/python3

from google.cloud import bigquery
import json

def query_stackoverflow():
    client = bigquery.Client()
    query_job = client.query(
        """
        SELECT u.display_name, AVG(c.score) as avg_score
        FROM `bigquery-public-data.stackoverflow.comments` as c
        INNER JOIN `bigquery-public-data.stackoverflow.users` as u ON u.id = c.id
        GROUP BY u.display_name 
        ORDER BY avg_score DESC
        LIMIT 10"""
    )

    results = query_job.result()
    data = {}
    for row in results:
          data[str(row[0])] = row[1]
    with open("result.json", "w") as f:
       json.dump(data, f)

if __name__ == "__main__":
    query_stackoverflow()