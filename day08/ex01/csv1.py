#! /usr/bin/python3

from google.cloud import bigquery
import csv

def query_stackoverflow():
    client = bigquery.Client()
    query_job = client.query(
        """
        SELECT user_display_name, score
        FROM `bigquery-public-data.stackoverflow.comments`
        ORDER BY score DESC
        LIMIT 10"""
    )

    results = query_job.result()
    header = ["user_display_name", "highest_score"]
    with open("result.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(header)
        for row in results:
           writer.writerow(row)

if __name__ == "__main__":
    query_stackoverflow()