#! /usr/bin/python3

from google.cloud import bigquery
import csv

def query_stackoverflow():
    client = bigquery.Client()
    query_job = client.query(
        """
        SELECT title, body, tags
        FROM `bigquery-public-data.stackoverflow.posts_questions`
        WHERE tags LIKE '%python%'
        """
    )

    results = query_job.result()
    header = [" post_title", "body", "all_tags"]
    with open("result.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(header)
        for row in results:
           writer.writerow(row)

if __name__ == "__main__":
    query_stackoverflow()