#! /usr/bin/python3

from google.cloud import bigquery

def query_stackoverflow():
    client = bigquery.Client()
    query_job = client.query(
        """
        SELECT u.display_name, count(c.id) as avg_score
        FROM `bigquery-public-data.stackoverflow.comments` as c
        INNER JOIN `bigquery-public-data.stackoverflow.users` as u ON u.id = c.id
        GROUP BY u.display_name 
        ORDER BY avg_score DESC
        LIMIT 10"""
    )

    results = query_job.result()
    with open("result.txt", "w") as f:
        for row in results:
            f.write(str(row[0] + " -> " + str(row[1]) + " comments"))
            f.write("\n")

if __name__ == "__main__":
    query_stackoverflow()