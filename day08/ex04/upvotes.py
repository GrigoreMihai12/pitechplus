#! /usr/bin/python3

from google.cloud import bigquery
import json

def query_stackoverflow():
    client = bigquery.Client()
    query_job = client.query(
        """
        SELECT display_name, up_votes, website_url
        FROM `bigquery-public-data.stackoverflow.users`
        ORDER BY up_votes DESC
        LIMIT 15"""
    )

    results = query_job.result()
    data = {}
    for row in results:
          data[str(row[0])] = [row[1], row[2]]
    with open("result.json", "w") as f:
       json.dump(data, f)

if __name__ == "__main__":
    query_stackoverflow()