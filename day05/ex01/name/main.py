import os
from flask import Flask
app = Flask(__name__)
@app.route("/hello_to_training/<name>")
def user(name):
    return f"<h1> Hy {name}! </h1>"
if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))
