HTTP is a protocol for fetching resources such as HTML documents. It is the foundation of any data exchange on the Web
and it is a client-server protocol, which means requests are initiated by the recipient, usually the Web browser. A complete document 
is reconstructed from the different sub-documents fetched, for instance, text, layout description, images, videos, scripts, and more.
lients and servers communicate by exchanging individual messages (as opposed to a stream of data). The messages sent by the client, usually a Web browser,
are called requests and the messages sent by the server as an answer are called responses.
HTTP is a client-server protocol: requests are sent by one entity, the user-agent (or a proxy on behalf of it). 
Most of the time the user-agent is a Web browser, but it can be anything, for example, a robot that crawls the Web to populate and maintain a search engine index.

PyCharm provides integration with the major means of requirements management and makes it possible to track the unsatisfied requirements in your 
projects and create a virtual environment based on the requirements.txt file.
We generate and share requirements.txt files to make it easier for other developers to install the correct versions of the required Python libraries
(or packages) to run the Python code we’ve written.