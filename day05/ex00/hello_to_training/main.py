import os
from flask import Flask, render_template, url_for
app = Flask(__name__, template_folder='templates')
@app.route("/")
def hello_world():
    return render_template("/home/griggmihai/pitechplus/day05/ex00/hello_to_training/app/templates/index.html")
if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))
