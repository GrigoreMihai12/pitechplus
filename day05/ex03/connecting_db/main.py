from flask import Flask, jsonify, request
from google.cloud import datastore
app = Flask(__name__)

datastore_client = datastore.Client()

tasks = [
    {
        'name':"item_name",
        'price':5
    },
    {
        'name':"item_name1",
        'price':5
    }
]

datastore_client = datastore.Client()

@app.route('/api/list', methods=['GET'])
def get_tasks():
    #return jsonify({'tasks': tasks})
    query = datastore_client.query()
    all_keys = query.fetch()
    list1 = []
    for keys in all_keys:
       list1.append(datastore_client.get(keys))
    return list1

@app.route('/api/add', methods=['POST'])
def create_task():
    #if not request.json or not 'name' in request.json:
        #return 404
    task = {
        'name': tasks[-1]['name'],
        'price': request.json['price']
    }
    task_key = datastore_client.key(task)
    task2 = datastore.Entity(key=task_key)
    datastore_client.put(task2)
    #tasks.append(task)
    #return jsonify({'task': task}), 201
    return 201


if __name__ == "__main__":
    app.run(debug=True)