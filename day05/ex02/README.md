HTTP/0.9 — The One-line Protocol
Initial version of HTTP — a simple client-server, request-response, telenet-friendly protocol
Request nature: single-line 
Methods supported: GET only
Response type: hypertext only

HTTP/1.0 — Building extensibility
Browser-friendly protocol
Provided header fields including rich metadata about both request and response
Response: not limited to hypertext (Content-Type header provided ability to transmit files other than plain HTML files)
Methods supported: GET , HEAD , POST

HTTP/1.1 — The standardized protocol
This is the HTTP version currently in common use.
Introduced critical performance optimizations and feature enhancements — persistent and pipelined connections, chunked transfers, compression/decompression, content negotiations, virtual hosting , faster response and great bandwidth savings by adding cache support.
Methods supported: GET , HEAD , POST , PUT , DELETE , TRACE , OPTIONS
Connection nature: long-lived