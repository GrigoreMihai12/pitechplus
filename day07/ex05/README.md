The point that distinguishes OLTP and OLAP is that OLTP is an online transaction system whereas, OLAP is an online data retrieval and analysis system.
Online transactional data becomes the source of data for OLTP. However, the different OLTPs database becomes the source of data for OLAP.
OLTP’s main operations are insert, update and delete whereas, OLAP’s main operation is to extract multidimensional data for analysis.
OLTP has short but frequent transactions whereas, OLAP has long and less frequent transaction.
Processing time for the OLAP’s transaction is more as compared to OLTP.
OLAPs queries are more complex with respect OLTPs.
The tables in OLTP database must be normalized (3NF) whereas, the tables in OLAP database may not be normalized.
As OLTPs frequently executes transactions in database, in case any transaction fails in middle it may harm data’s integrity and hence it must take care of data integrity. While in OLAP the transaction is less frequent hence, it does not bother much about data integrity.