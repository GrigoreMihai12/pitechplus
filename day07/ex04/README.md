Google BigQuery: Analyze terabytes of data in seconds. Run super-fast, SQL-like queries against terabytes of data in seconds, using the processing power of Google's infrastructure Load data with ease. Bulk load your data using Google Cloud Storage or stream it in. Easy access. Access BigQuery by using a browser tool, a command-line tool, or by making calls to the BigQuery REST API with client libraries such as Java, PHP or Python.;
Google Cloud Datastore: A Fully Managed NoSQL Data Storage Service. Use a managed, NoSQL, schemaless database for storing non-relational data. Cloud Datastore automatically scales as you need it and supports transactions as well as robust, SQL-like queries.
Google BigQuery belongs to "Big Data as a Service" category of the tech stack, while Google Cloud Datastore can be primarily classified under "NoSQL Database as a Service".
Some of the features offered by Google BigQuery are:
All behind the scenes- Your queries can execute asynchronously in the background, and can be polled for status.
Import data with ease- Bulk load your data using Google Cloud Storage or stream it in bursts of up to 1,000 rows per second.
Affordable big data- The first Terabyte of data processed each month is free.
On the other hand, Google Cloud Datastore provides the following key features:
Schemaless access, with SQL-like querying
Managed database
Autoscale with your users