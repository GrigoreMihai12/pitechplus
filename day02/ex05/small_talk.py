#! /usr/bin/python3

def summary_dict(string_data):
    dic = { x:string_data.splitlines().split(" ")[i + 1] for i, x in enumerate(string_data.split(" ")) if x == "sender:" or x == "message:" or x == "reciver:"}
    return dic

string_data = "id_00 sender: Maria message: “hello Gina, How are you?” receiver: Gina \n id_01 sender: Dorin message: ”Good Morning” receiver: Nicu"
print(summary_dict(string_data))
