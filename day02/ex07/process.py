#! /usr/bin/python3
import multiprocessing
import random
import time
def worker(name: str) -> None:
    print(f'Started worker {name}')
    worker_time = 40
    time.sleep(worker_time)
    print('Gata!')
processes = []
for i in range(5):
    process = multiprocessing.Process(target=worker, args=(f'computer_{i}',))
    processes.append(process)
    process.start()
    for proc in processes:
        proc.join()
