Dictionary comprehensions are just like list comprehensions, except that you group the expression using curly braces instead of square braces. Also, the left part before the for keyword
 expresses both a key and a value, separated by a colon. The notation is specifically designed to remind you of list comprehensions as applied to dictionaries.

