#! /usr/bin/python3

def mix_lists(list1, list2, list3, list4):
    print(list1)
    print(list2)
    print(list3)
    print(list4)
    returned_list = [  {list1[i]:{"key_2":list2[i], "key_3":list3[i], "key_4":list4[i]}} for i in (0, len(list1)-1)]
    return returned_list
list1 = ["my_key_name1", 3]
list2 = ["green", "brown", "blue"]
list3 = [1,2,3,4]
list4 = ["string", "float", -1, 0]
print(mix_lists(list1, list2, list3, list4))
