List comprehension offers a shorter syntax when you want to create a new list based on the values of an existing list and not only.
Every list comprehension in Python includes three elements:
1.Expression is the member itself is the member itself, a call to a method or any other valid expression that retrns a value
2.Member is the object or value in the list or iterable.
3.Iterable is a list, set, sequence, generator or any other object that can return its elements one at a time.
