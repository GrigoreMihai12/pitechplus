#! /usr/bin/python3

import _thread
import time

def th1():
    print("thread1")
    time.sleep(60)

def th2():
    print("thread2")
    time.sleep(120)

def th3():
    print("thread3")
    time.sleep(180)

def th4():
    print("thread4")
    time.sleep(240)

def th5():
    print("thread5")
    time.sleep(300)

args = ()
_thread.start_new_thread(th1,args)
_thread.start_new_thread(th2,args)
_thread.start_new_thread(th3,args)
_thread.start_new_thread(th4,args)
_thread.start_new_thread(th5,args)

while 1:
    pass
